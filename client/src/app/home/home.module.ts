import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../shared/material/material.module';

import { HomeComponent } from '../home/home.component';
import { SocketService } from '../chat/shared/services/socket.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [HomeComponent],
  providers: [SocketService],
  entryComponents: [HomeComponent]
})
export class HomeModule { }
