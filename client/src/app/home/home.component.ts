import { Component, OnInit, ViewChildren, ViewChild, AfterViewInit, QueryList, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MatList, MatListItem } from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import { Routes, RouterModule, Router  } from '@angular/router';

import { Action } from '../chat/shared/model/action';
import { Event } from '../chat/shared/model/event';
import { Message } from '../chat/shared/model/message';
import { User } from '../chat/shared/model/user';
import { SocketService } from '../chat/shared/services/socket.service';

const AVATAR_URL = 'https://api.adorable.io/avatars/285';
@Component({
  selector: 'tcc-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'], providers: [SocketService]
})
export class HomeComponent implements OnInit {
  action = Action;
  user: User;
  ioConnection: any;
  connectedClientsCount: number;

  constructor(private socketService: SocketService, private router: Router) { }

  ngOnInit() {
    this.initModel();
  }
  private initModel(): void {
    const randomId = this.getRandomId();
    this.user = {
      id: randomId
    };
  }

  private getRandomId(): number {
    return Math.floor(Math.random() * (1000000)) + 1;
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    this.socketService.onEvent(Event.CONNECT)
      .subscribe(() => {
        console.log('connected');
      });

    this.socketService.onEvent(Event.DISCONNECT)
      .subscribe(() => {
        console.log('disconnected');
      });
  }

  public initiateConnection(): void {
    this.router.navigate(['chat']);
  }

}
