import { createServer, Server } from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';

import { Message } from './model/message';

export class ChatServer {
    public static readonly PORT:number = 8080;
    private app: express.Application;
    private server: Server;
	private connectedClientsCount: number;
    private io: SocketIO.Server;
    private port: string | number;

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.listen();
    }

    private createApp(): void {
		this.connectedClientsCount = 0;
        this.app = express();
    }

    private createServer(): void {
        this.server = createServer(this.app);
    }

    private config(): void {
        this.port = process.env.PORT || ChatServer.PORT;
    }

    private sockets(): void {
        this.io = socketIo(this.server);
    }

    private listen(): void {
        this.server.listen(this.port, () => {
            console.log('Running server on port %s', this.port);
        });

        this.io.on('connect', (socket: any) => {
            console.log('Connected client on port %s.', this.port);
			this.connectedClientsCount++;
            console.log('Connected connectedClientsCount' + this.connectedClientsCount);
            var srvSockets = this.io.sockets.sockets;
            console.log('Object.keys(srvSockets).length ' + Object.keys(srvSockets).length);
            //console.log('srvSockets', srvSockets);
            console.log('Connected client on port %s.', this.port);
            socket.on('message', (m: Message) => {
                console.log('[server](message): %s', JSON.stringify(m));
                  m.connectedClientsCount = this.connectedClientsCount;
                this.io.emit('message', m);
            });

            socket.on('disconnect', () => {
				this.connectedClientsCount--;
                console.log('Client disconnected');
				console.log('disconnected - connectedClientsCount'+this.connectedClientsCount);
            });
        });
    }

    public getApp(): express.Application {
        return this.app;
    }
}
